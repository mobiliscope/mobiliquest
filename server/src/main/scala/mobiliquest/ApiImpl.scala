package mobiliquest

import shared.data
import shared.data.*
import shared.data.{Indicators, RequestResponse, emptyResponse}
import better.files.*
import org.http4s.{StaticFile, Status}

import java.nio.file.Files
import scala.concurrent.duration.*
import scala.language.postfixOps
import scala.concurrent.Await
import scala.concurrent.ExecutionContext.Implicits.*

class ApiImpl(configs: Map[String, Config]) {

  def run(request: data.Request): RequestResponse = {

    try {
      configs.get(request.environment) match {
        case Some(config: Config) =>
          val fileService = new FileService(config)
          fileService.withSourceData[RequestResponse] { inputDir =>
            fileService.withTmpDir[RequestResponse] { outputDir =>
              val hash = Serializer.toJson(VersionedRequest.fromRequest(request, config.majorVersion, config.minorVersion), outputDir, "filters.json")
              val outputDirWithHash = s"$outputDir/$hash".toFile
              val minioPath = s"${fileService.outDataFolder}/$hash"
              println("miniopath " + minioPath)
              val url = fileService.getURL(minioPath)
              val p2mCall = R.getP2mCall(request, s"${inputDir.pathAsString}/${fileService.sourceDataFolder}", outputDirWithHash.pathAsString)
              println("URL " + url)
              if (!fileService.existsDir(minioPath)) {
                val rFuture = ThreadService.submit(() => R.computeAll(p2mCall))
                for {
                  valid <- rFuture
                } yield {
                  if (valid)
                    fileService.uploadOutData(outputDirWithHash.listRecursively.toSeq, outputDirWithHash.pathAsString, hash)
                }
              }
              Mailing.send(config, request.userEmail, url)
              RequestResponse.Ok(
                if (!fileService.existsDir(minioPath)) p2mCall
                else minioPath
              )
            }
          }
        case _ =>
          val message = "No matching settings found for " + request.environment
          println(message)
          RequestResponse.BadRequest(message)
      }
    } catch {
      case x: Any =>
        val message = "Mobiliquest process errors:  " + x
        println(message)
        RequestResponse.BadRequest(message)
    }
  }

}
