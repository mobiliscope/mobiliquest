package mobiliquest

import org.http4s.StaticFile
import shared.data.*
import org.http4s.*
import org.http4s.dsl.io.*
import shared.data.RequestType._

object R {
  lazy val R = org.ddahl.rscala.RClient()
  lazy val api = scala.io.Source.fromResource("routines/p2m_fct.R").getLines().mkString("\n")

  def filterFullModalities(study: Study, indicatorName: IndicatorName, flattenModalities: Seq[Modality]) =
    if (flattenModalities.length == shared.Utils.flatten(Indicators.availableIndicatorsAndModalities(study)(indicatorName)).length) Seq()
    else flattenModalities

  def getP2mCall(request: shared.data.Request, inputDir: Directory, outputDir: Directory) = 
    println("compute all " + request.study)
    val Rfilters = "list(" + s""" "${request.filter}" = c(${request.modalities.mkString(", ")}) """ + ")"

    def indicatorList(rType: RequestType) =
      if (rType == request.requestType) Rfilters
      else "list()"

    s"""\np2m("${request.study}", ${indicatorList(Perimeter)}, ${indicatorList(Subpop)}, "$inputDir", "$outputDir", 0)"""
    
  def computeAll(p2mCall: String) = 
    println("call :  " +  p2mCall)
    R.evalL0(api + p2mCall)
}
