package mobiliquest

import cats.effect.*
import cats.syntax.all.*
import com.comcast.ip4s.*
import org.http4s.ember.server.*
import org.http4s.implicits.*
import org.http4s.server.Router

import better.files.*
import scala.concurrent.duration.*
import scala.concurrent.ExecutionContext.global
import scala.concurrent.duration.Duration

object Main extends IOApp {


  override def run(args: List[String]): IO[ExitCode] = {

    val confiFilePath = args.headOption match {
      case Some(path: String) => File(path) // pass in java command line
      case _ => File("/data/config/settings.json") // copied in docker
    }

    val json = confiFilePath.lines.mkString("\n")
    val configs = Config.fromJson(json)

    val apiImpl = new ApiImpl(configs)
    val rootPage = new RootPage(apiImpl)

    val httpApp = Router("/" -> rootPage.routes).orNotFound
    
    EmberServerBuilder
    .default[IO]
    .withHost(host"0.0.0.0")
    .withPort(port"9002")
    .withHttpApp(httpApp)
    .build
    .use(_ => IO.never)
    .as(ExitCode.Success)

  }

}

