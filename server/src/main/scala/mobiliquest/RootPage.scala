package mobiliquest

import cats.effect.*
import com.comcast.ip4s.*
import org.http4s.*
import org.http4s.dsl.io.*
import org.http4s.implicits.*
import io.circe.generic.auto.*
import io.circe.syntax.*
import org.http4s.circe.*
import org.http4s.circe.CirceEntityCodec.circeEntityDecoder
import shared.data
import shared.data.RequestType._


class RootPage(apiImpl: ApiImpl) {

  val routes = HttpRoutes.of[IO] {
    case req@POST -> Root / "request" =>
      for {
        // Decode a User request
        request <- req.as[data.Request]
        requestResponse = apiImpl.run(request)
        // Encode a hello response
        resp <- Ok(requestResponse.asJson)
      } yield (resp)
  }

}

