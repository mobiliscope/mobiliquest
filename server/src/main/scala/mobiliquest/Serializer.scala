package mobiliquest


import org.json4s.JsonDSL._
import org.json4s.jackson.JsonMethods._
import shared.data._
import better.files._
import io.circe.generic.auto.*
import io.circe.syntax.*
import org.http4s.circe.*
import org.http4s.circe.CirceEntityCodec.circeEntityDecoder

object Serializer {

  def toJson(request: VersionedRequest, target: File, fileName: String) = {

//    val json =
//      ("request" ->
//        ("study" -> request.study) ~
//          ( "requestType" -> request.requestType.toString) ~
//          ("indicator" -> request.filterIndicator) ~
//          ("modalities" -> request.modalities)// -> request.filterIndicator -> request.modalities)
//        )

    val jsonText = request.asJson.noSpaces
      //pretty(render(json))
    val hash = Utils.hash(jsonText)
    val outPath = s"$target/$hash".toFile

    outPath.toJava.mkdir
    s"$outPath/$fileName".toFile.overwrite(jsonText)
    hash
  }

}