package mobiliquest

import shared.data.Indicator
//import org.http4s.circe._
import io.circe._, io.circe.generic.auto._, io.circe.parser._, io.circe.syntax._


object RequestUtils {


  case class User(name: String)


  implicit class ARequest(u: User) {

    def toJson: String = u.asJson.noSpaces

  }
}
