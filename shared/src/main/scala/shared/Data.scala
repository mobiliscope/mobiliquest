package shared

import shared.data.RequestType.{Perimeter, Subpop}
import io.circe.derivation.{Configuration, ConfiguredEnumCodec}

import javax.lang.model.element.NestingKind
import scala.scalajs.js.|


object data {

  type Study = String
  type Directory = String
  type Modality = Int
  type IndicatorName = String
  type Modalities = Seq[Either[Modality, Seq[Modality]]]
  type Environment = String

  given Configuration = Configuration.default

  enum RequestType derives ConfiguredEnumCodec:
    case Subpop, Perimeter

  case class Indicator(RName: IndicatorName, description: String, modalityDescriptions: Seq[(Modality, String)], requestType: RequestType)

  type IndicatorAndModalities = Map[IndicatorName, Modalities]

  implicit def intToLeftModality(m: Int): Left[Modality, Seq[Modality]] = Left(m)

  implicit def seqOfIntToLeftModality(ms: Seq[Int]): Right[Modality, Seq[Modality]] = Right(ms)

  // implicit def ModalityToLeftModality(m: Seq[Modality]): Seq[Left[Modality, Seq[Modality]]] = m.map{Left(_)}

  implicit def ModalityToRightModality(m: Seq[Seq[Modality]]): Seq[Right[Modality, Seq[Modality]]] = m.map {
    Right(_)
  }

  case class Request(study: Study, filter: IndicatorName, modalities: Seq[Modality], requestType: RequestType, environment: String, userEmail: String)

  case class VersionedRequest(study: Study,
                              filter: IndicatorName,
                              modalities: Seq[Modality],
                              requestType: RequestType,
                              environment: String,
                              userEmail: String,
                              majorVersion: String,
                              minorVersion: String)

  //case class RequestResponse(filterURL: Option[String], statURL: Option[String])
  enum RequestResponse:
    case Ok(message: String)
    case BadRequest(error: String)

  object VersionedRequest {
    def fromRequest(request: Request, majorVersion: String, minorVersion: String) =
      VersionedRequest(request.study,
        request.filter,
        request.modalities,
        request.requestType,
        request.environment,
        request.userEmail,
        majorVersion,
        minorVersion)
  }

  val emptyResponse = RequestResponse.Ok

  sealed trait RequestStatus

  case object Off extends RequestStatus

  case object Running extends RequestStatus

  case class Done(requestResponse: RequestResponse) extends RequestStatus

  object Indicators {

    val perimetre = Indicator(
      "PERIM",
      "Perimeter according to urban area zoning",
      Seq(
        1 -> "Peripheral areas",
        2 -> "Urban areas",
        3 -> "Inner city",
        4 -> "Distant periphery",
        5 -> "Close periphery",
        6 -> "Pericenter",
        7 -> "Center"
      ),
      Perimeter
    )

    val kAge = Indicator(
      "KAGE",
      "Age groups",
      Seq(
        0 -> "15 and less",
        1 -> "16-24",
        2 -> "25-34",
        3 -> "35-64",
        4 -> "65 and more"
      ),
      Subpop
    )

    val kAge_ca = Indicator(
      "KAGE",
      "Age groups",
      Seq(
        0 -> "14 and less",
        1 -> "15-24",
        2 -> "25-34",
        3 -> "35-64",
        4 -> "65 and more"
      ),
      Subpop
    )

    val sex = Indicator(
      "SEX",
      "Sex",
      Seq(
        1 -> "Male",
        2 -> "Female"
      ),
      Subpop
    )

    val strM_fr = Indicator(
      "STRM",
      "Household composition",
      Seq(
        1 -> "Single-person household",
        2 -> "Couple without children",
        3 -> "Household (excluding couple) without children",
        4 -> "Household with children"
      ),
      Subpop
    )

    val strM_ca = Indicator(
      "STRM",
      "Household composition",
      Seq(
        1 -> "Single-person household",
        2 -> "Household without children",
        3 -> "Household with children"
      ),
      Subpop
    )

    val strM_al = Indicator(
      "STRM",
      "Household composition",
      Seq(
        1 -> "Single-person household",
        2 -> "Family without children",
        3 -> "Complex household without children",
        4 -> "Family with children",
        5 -> "Complex household with children"
      ),
      Subpop
    )

    val educ = Indicator(
      "EDUC",
      "Educational level (individual)",
      Seq(
        1 -> "Low",
        2 -> "Intermediate",
        3 -> "High",
        4 -> "Very high"
      ),
      Subpop
    )

    val educMen = Indicator(
      "EDUCMEN",
      "Educational level (household)",
      Seq(
        1 -> "Low",
        2 -> "Intermediate",
        3 -> "High",
        4 -> "Very high"
      ),
      Subpop
    )

    val rev = Indicator(
      "REV",
      "Household income",
      Seq(
        1 -> "Low",
        2 -> "Middle-low",
        3 -> "Middle-high",
        4 -> "High",
        5 -> "Unknown"
      ),
      Subpop
    )

    val rev_al = Indicator(
      "REV",
      "Household income",
      Seq(
        1 -> "Very low",
        2 -> "Low",
        3 -> "Intermediate",
        4 -> "High",
        5 -> "Very high"
      ),
      Subpop
    )

    val cso = Indicator(
      "CSO",
      "Socioprofessional status",
      Seq(
        1 -> "Unskilled workers",
        2 -> "Skilled workers",
        3 -> "Self-employed",
        4 -> "Executives and professionals"
      ),
      Subpop
    )

    val inf = Indicator(
      "INFORMAL",
      "Professional informality",
      Seq(
        1 -> "Formal workers",
        2 -> "Informal workers",
      ),
      Subpop
    )

    val sse = Indicator(
      "SSE",
      "Socio-economic stratum of residence",
      Seq(
        1 -> "Stratum 1 or not stratified",
        2 -> "Stratum 2",
        3 -> "Stratum 3",
        4 -> "Stratum 4, 5 or 6"
      ),
      Subpop
    )

    val log = Indicator(
      "LOG",
      "Housing tenure",
      Seq(
        1 -> "Rent-free",
        2 -> "Tenants",
        3 -> "Owners"
      ),
      Subpop
    )

    val csp = Indicator(
      "CSP",
      "Socioprofessional status (individual)",
      Seq(
        1 -> "Inactive",
        2 -> "Workers",
        3 -> "Employees",
        4 -> "Intermediate occupants",
        5 -> "Managers and intellectual professionals"
      ),
      Subpop
    )

    val cspMen = Indicator(
      "CSPMEN",
      "Socioprofessional status (household)",
      Seq(
        1 -> "Inactive",
        2 -> "Workers",
        3 -> "Employees",
        4 -> "Intermediate occupants",
        5 -> "Managers and intellectual professionals"
      ),
      Subpop
    )

    val occ = Indicator(
      "OCC",
      "Occupational status",
      Seq(
        1 -> "Active",
        2 -> "Student",
        3 -> "Unemployed",
        4 -> "Retired",
        5 -> "Inactive"
      ),
      Subpop
    )

    val dep = Indicator(
      "DEP",
      "Departement de residence",
      Seq(
        1 -> "Paris",
        2 -> "Seine-Saint-Denis",
        3 -> "Val-de-Marne",
        4 -> "Haut-de-Seine",
        5 -> "Greater Paris"
      ),
      Subpop
    )

    val zonage = Indicator(
      "ZONAGE",
      "Residential location in the urban/peripheral rings",
      Seq(
        1 -> "Peripheral areas",
        2 -> "Urban areas",
        3 -> "Inner city",
        4 -> "Distant periphery",
        5 -> "Close periphery",
        6 -> "Pericenter",
        7 -> "Center"
      ),
      Subpop
    )

    val qpv = Indicator(
      "QPV",
      "Residential location in/outside 'Poverty Areas' (QPV)",
      Seq(
        1 -> "Inside QPV",
        2 -> "Outside QPV"
      ),
      Subpop
    )

    val basic: IndicatorAndModalities = Map(
      kAge.RName -> Seq(1, 2, 3, 4),
      sex.RName -> Seq(1, 2),
      strM_fr.RName -> Seq(1, 2, 3, 4),
      educ.RName -> Seq(1, 2, 3, 4),
      educMen.RName -> Seq(1, 2, 3, 4),
      csp.RName -> Seq(1, 2, 3, 4, 5),
      cspMen.RName -> Seq(1, 2, 3, 4, 5),
      occ.RName -> Seq(1, 2, 3, 4, 5),
      zonage.RName -> Seq(1, 2, 3),
      qpv.RName -> Seq(1, 2)
    )

    val annecy: IndicatorAndModalities = Map(
      kAge.RName -> Seq(1, 2, 3, 4),
      sex.RName -> Seq(1, 2),
      strM_fr.RName -> Seq(1, 2, 3, 4),
      educ.RName -> Seq(1, 2, 3, 4),
      educMen.RName -> Seq(1, 2, 3, 4),
      csp.RName -> Seq(1, 2, 3, 4, 5),
      cspMen.RName -> Seq(1, 2, 3, 4, 5),
      occ.RName -> Seq(1, 2, 3, 4, 5),
      zonage.RName -> Seq(1, 2, 3)
    )

    val besCarc: IndicatorAndModalities = Map(
      kAge.RName -> Seq(1, 2, 3, 4),
      sex.RName -> Seq(1, 2),
      strM_fr.RName -> Seq(1, 2, 3, 4),
      educ.RName -> Seq(1, 2, 3, 4),
      educMen.RName -> Seq(1, 2, 3, 4),
      csp.RName -> Seq(1, 2, 3, 4, 5),
      cspMen.RName -> Seq(1, 2, 3, 4, 5),
      occ.RName -> Seq(1, 2, 3, 4, 5),
      zonage.RName -> Seq(1, 3),
      qpv.RName -> Seq(1, 2)
    )

    val idf: IndicatorAndModalities = Map(
      kAge.RName -> Seq(1, 2, 3, 4),
      sex.RName -> Seq(1, 2),
      strM_fr.RName -> Seq(1, 2, 3, 4),
      educ.RName -> Seq(1, 2, 3, 4),
      educMen.RName -> Seq(1, 2, 3, 4),
      rev.RName -> Seq(1, 2, 3, 4),
      csp.RName -> Seq(1, 2, 3, 4, 5),
      cspMen.RName -> Seq(1, 2, 3, 4, 5),
      occ.RName -> Seq(1, 2, 3, 4, 5),
      dep.RName -> Seq(1, 2, 3, 4, 5),
      zonage.RName -> Seq(1, 2, 3),
      qpv.RName -> Seq(1, 2)
    )

    val bogota: IndicatorAndModalities = Map(
      kAge.RName -> Seq(1, 2, 3, 4),
      sex.RName -> Seq(1, 2),
      strM_al.RName -> Seq(1, 2, 3, 4, 5),
      educ.RName -> Seq(1, 2, 3, 4),
      educMen.RName -> Seq(1, 2, 3, 4),
      rev_al.RName -> Seq(1, 2, 3, 4, 5),
      cso.RName -> Seq(1, 2, 3, 4),
      inf.RName -> Seq(1, 2),
      occ.RName -> Seq(1, 2, 3, 4, 5),
      zonage.RName -> Seq(4, 5, 6, 7),
      sse.RName -> Seq(1, 2, 3, 4),
      log.RName -> Seq(1, 2, 3)
    )

    val santiago: IndicatorAndModalities = Map(
      kAge_ca.RName -> Seq(1, 2, 3, 4),
      sex.RName -> Seq(1, 2),
      strM_al.RName -> Seq(1, 2, 3, 4, 5),
      educ.RName -> Seq(1, 2, 3, 4),
      educMen.RName -> Seq(1, 2, 3, 4),
      rev_al.RName -> Seq(1, 2, 3, 4, 5),
      cso.RName -> Seq(1, 2, 3, 4),
      occ.RName -> Seq(1, 2, 3, 4, 5),
      zonage.RName -> Seq(4, 5, 6, 7),
      log.RName -> Seq(1, 2, 3)
    )

    val saopaulo: IndicatorAndModalities = Map(
      kAge.RName -> Seq(1, 2, 3, 4),
      sex.RName -> Seq(1, 2),
      strM_al.RName -> Seq(1, 2, 3, 4, 5),
      educ.RName -> Seq(1, 2, 3, 4),
      educMen.RName -> Seq(1, 2, 3, 4),
      rev_al.RName -> Seq(1, 2, 3, 4, 5),
      cso.RName -> Seq(1, 2, 3, 4),
      inf.RName -> Seq(1, 2),
      occ.RName -> Seq(1, 2, 3, 4, 5),
      zonage.RName -> Seq(4, 5, 6, 7),
      log.RName -> Seq(1, 2, 3)
    )


    val canadians: IndicatorAndModalities = Map(
      kAge.RName -> Seq(1, 2, 3, 4),
      sex.RName -> Seq(1, 2),
      strM_ca.RName -> Seq(1, 2, 3),
      rev.RName -> Seq(1, 2, 3, 4, 5),
      occ.RName -> Seq(1, 2, 3, 4, 5)
    )

    val spatialFR: IndicatorAndModalities = Map(perimetre.RName -> Seq(Seq(3, 2, 1), Seq(3, 2), Seq(3)))
    val spatialCaBe: IndicatorAndModalities = Map(perimetre.RName -> Seq(Seq(3, 1), Seq(3)))
    val spatialAL: IndicatorAndModalities = Map(perimetre.RName -> Seq(Seq(7, 6, 5, 4), Seq(7, 6, 5), Seq(7, 6)))
    val spatialNone: IndicatorAndModalities = Map(perimetre.RName -> Seq())

    val availableIndicatorsAndModalities: Map[Study, IndicatorAndModalities] = Map(
      "albi" -> (basic ++ spatialFR),
      "alencon" -> (basic ++ spatialFR),
      "amiens" -> (basic ++ spatialFR),
      "annecy" -> (annecy ++ spatialFR),
      "angers" -> (basic ++ spatialFR),
      "angouleme" -> (basic ++ spatialFR),
      "annemasse" -> (basic ++ spatialFR),
      "bayonne" -> (basic ++ spatialFR),
      "besancon" -> (besCarc ++ spatialCaBe),
      "beziers" -> (basic ++ spatialFR),
      "bogota" -> (bogota ++ spatialAL),
      "bordeaux" -> (basic ++ spatialFR),
      "brest" -> (basic ++ spatialFR),
      "caen" -> (basic ++ spatialFR),
      "carcassonne" -> (besCarc ++ spatialCaBe),
      "cherbourg" -> (basic ++ spatialFR),
      "clermont-ferrand" -> (basic ++ spatialFR),
      "creil" -> (basic ++ spatialFR),
      "dijon" -> (basic ++ spatialFR),
      "douai" -> (basic ++ spatialFR),
      "dunkerque" -> (basic ++ spatialFR),
      "grenoble" -> (basic ++ spatialFR),
      "idf" -> (idf ++ spatialFR),
      "la-reunion" -> (basic ++ spatialFR),
      "la-rochelle" -> (basic ++ spatialFR),
      "le-havre" -> (basic ++ spatialFR),
      "lille" -> (basic ++ spatialFR),
      "longwy" -> (basic ++ spatialFR),
      "lyon" -> (basic ++ spatialFR),
      "marseille" -> (basic ++ spatialFR),
      "martinique" -> (basic ++ spatialFR),
      "metz" -> (basic ++ spatialFR),
      "montpellier" -> (basic ++ spatialFR),
      "montreal" -> (canadians ++ spatialFR),
      "nancy" -> (basic ++ spatialFR),
      "nantes" -> (basic ++ spatialFR),
      "nice" -> (basic ++ spatialFR),
      "nimes" -> (basic ++ spatialFR),
      "niort" -> (basic ++ spatialFR),
      "ottawa-gatineau" -> (canadians ++ spatialNone),
      "poitiers" -> (basic ++ spatialFR),
      "quebec" -> (basic ++ spatialFR),
      "quimper" -> (basic ++ spatialFR),
      "rennes" -> (basic ++ spatialFR),
      "rouen" -> (basic ++ spatialFR),
      "saguenay" -> (canadians ++ spatialNone),
      "saint-brieuc" -> (basic ++ spatialFR),
      "saint-etienne" -> (basic ++ spatialFR),
      "santiago" -> (santiago ++ spatialAL),
      "sao-paulo" -> (saopaulo ++ spatialAL),
      "sherbrook" -> (canadians ++ spatialFR),
      "strasbourg" -> (basic ++ spatialFR),
      "thionville" -> (basic ++ spatialFR),
      "toulouse" -> (basic ++ spatialFR),
      "tours" -> (basic ++ spatialFR),
      "trois-riviere" -> (canadians ++ spatialNone),
      "valence" -> (basic ++ spatialFR),
      "valenciennes" -> (basic ++ spatialFR)
    )
  }

}
