package mobiliquest

import com.mailjet.client.*
import com.mailjet.client.errors.MailjetException
import com.mailjet.client.transactional.*

object Mailing {

  def send(config: Config, userEmail: String, url: String) =

    println("Send ! " + config + config.mailjetApiKey)
    val options = ClientOptions.builder()
      .apiKey(config.mailjetApiKey)
      .apiSecretKey(config.mailjetSecretKey)
      .build()

    val client = new MailjetClient(options)

    val message = TransactionalEmail
      .builder()
      .to(new SendContact(userEmail, ""))
      .from(new SendContact(config.senderEmail, "Mobiliscope team"))
      .htmlPart(s"Hi, your mobility request is ready. View the results in the Mobiliscope: <a href=$url>$url</a>")
      .subject("Your Mobiliscope request is ready !")
      .trackOpens(TrackOpens.ENABLED)
      .build()

    val request = SendEmailsRequest
      .builder()
      .message(message)
      .build();

    try {
      request.sendWith(client)
    } catch {
      case e: MailjetException=> println("Error mailjet: " + e.getMessage + "\n" + e.getStackTrace.mkString("\n"))
      case x: Any=> println("Any: " + x)
    }
}
