package mobiliquest

import io.minio.*
import better.files.*
import io.minio.errors.MinioException
import io.minio.http.Method
import io.minio.messages.Item

import java.util.concurrent.TimeUnit
import scala.io.Source
import scala.jdk.CollectionConverters.IteratorHasAsScala
import scala.util.{Failure, Success, Try}

class FileService(config: Config) {

  val minioClient = MinioClient.builder
    .endpoint(config.minioEndPoint)
    .credentials(config.minioAccessKey, config.minioSecretKey)
    .build()

  val mobiliquestBucket = "mobiliquest"
  val versionSubFolders = s"${config.majorVersion}/${config.minorVersion}"
  val sourceDataFolder = s"source/$versionSubFolders"
  val outDataFolder = s"out-data/$versionSubFolders"

  createBucket(mobiliquestBucket)

  def withSourceData[T](op: File => T) =
    withTmpDir[T] { file =>
      downloadFolderContent(sourceDataFolder, file)
      op(file)
    }

  def withTmpDir[T](f: File ⇒ T) = {
    val file = java.nio.file.Files.createTempDirectory("MBQ").toFile.getAbsolutePath.toFile
    try {
      f(file)
    }
    finally {
      file.delete()
    }
  }
  
  def getURL(fileName: String) = {
    minioClient.getPresignedObjectUrl(
      GetPresignedObjectUrlArgs.builder()
        .method(Method.GET)
        .bucket(mobiliquestBucket)
        .`object`(s"$fileName")
        .expiry(2, TimeUnit.HOURS)
        .build
    )

  }

  def exists(bucketName: String) = {
    minioClient.bucketExists(BucketExistsArgs.builder.bucket(bucketName).build)
  }

  def existsDir(dirName: String) = {
    bucketContent(mobiliquestBucket).map { f =>
      f.get().objectName().split("/").dropRight(2)
    }.distinct.find { f =>
      f.mkString("/") == dirName
    } match {
      case Some(_) => true
      case _ => false
    }
  }


  def createBucket(name: String) = {
    if (!exists(name)) {
      minioClient.makeBucket(MakeBucketArgs.builder.bucket(name).build)
    }
  }

  type TryResult[T] = Either[T, String]

  def tryTo[T](op: () => T): TryResult[T] = {
    Try(op()) match {
      case Success(s) => Left(s)
      case Failure(ex) => ex match {
        case (e: MinioException) =>
          val text = e.getMessage + "\n" + e.httpTrace
          println("[MINI IO ERROR] " + text)
          Right(text)
        case (e: Throwable) =>
          val text = e.getMessage + "\n" + e.getStackTrace.mkString("\n")
          println("[ERROR] " + text)
          Right(text)
      }
    }
  }

  def bucketContent(name: String): Iterator[Result[Item]] = {
    if (exists(name)) {
      minioClient.listObjects(ListObjectsArgs.builder.recursive(true).bucket(name).build).iterator().asScala
    } else Iterator[Result[Item]]()
  }

  def createDirectory(bucket: String, directoryName: String) = {
    minioClient.listObjects(
      ListObjectsArgs.builder
        .bucket(bucket)
        .prefix(directoryName)
        .build())
  }

  def uploadFiles(files: Seq[File], skipPath: String, hash: String) = {
    createBucket(mobiliquestBucket)

    files.foreach { f =>
      val targetPath = s"${outDataFolder}/$hash/${f.pathAsString.replace(skipPath, "")}"
      if (f.isDirectory) createDirectory(mobiliquestBucket, targetPath)
      else {
        minioClient.uploadObject(
          UploadObjectArgs.builder
            .bucket(mobiliquestBucket)
            .`object`(targetPath)
            .filename(f.pathAsString).build)
      }
    }
  }

  def uploadOutData(files: Seq[File], skipPath: String, hash: String) = uploadFiles(files, skipPath, hash)

  def downloadFolderContent(filePath: String, target: File) = {
    tryTo(() =>
      bucketContent(mobiliquestBucket).foreach { result =>
        val objectName = result.get.objectName
        val fullTarget = target / objectName.split("/").dropRight(1).mkString("/")
        if (!fullTarget.exists) fullTarget.createDirectories
        minioClient.downloadObject(
          DownloadObjectArgs.builder
            .bucket(mobiliquestBucket)
            .filename(filePath)
            .`object`(result.get.objectName)
            .filename((target / objectName).pathAsString)
            .overwrite(true)
            .build
        )
      }
    )
  }


}
