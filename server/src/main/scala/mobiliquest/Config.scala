package mobiliquest

import io.circe.*
import io.circe.generic.semiauto.*
import io.circe.syntax.*
import io.circe.jawn.decode
import shared.data.Environment

// majorVersion: 4.3
// minorVersion: YYYYMMDD
// sources in source/versionType/version
case class Config(environment: Environment, mobiliscopeEndPoint: String, minioEndPoint: String, minioAccessKey: String, minioSecretKey: String, majorVersion: String, minorVersion: String, senderEmail: String, mailjetApiKey: String, mailjetSecretKey: String)

object Config {

  implicit val ConfigEncoder: Encoder[Config] =  new Encoder[Config] {
      final def apply(a: Config): Json = Json.obj(
        ("environment", Json.fromString(a.environment)),
        ("mobiliscopeEndPoint", Json.fromString(a.mobiliscopeEndPoint)),
        ("minioEndPoint", Json.fromString(a.minioEndPoint)),
        ("minioAccessKey", Json.fromString(a.minioAccessKey)),
        ("minioSecretKey", Json.fromString(a.minioSecretKey)),
        ("majorVersion", Json.fromString(a.majorVersion)),
        ("minorVersion", Json.fromString(a.minorVersion)),
        ("senderEmail", Json.fromString(a.senderEmail)),
        ("mailjetApiKey", Json.fromString(a.mailjetApiKey)),
        ("mailjetSecretKey", Json.fromString(a.mailjetSecretKey)),
      )
    }

  implicit val ConfigDecoder: Decoder[Config] = Decoder.instance { c =>
    val mobiEndPoint = c.downField("mobiliscopeEndPoint")
    val endPoint = c.downField("minioEndPoint")
    val accesK = c.downField("minioAccessKey")
    val secretK = c.downField("minioSecretKey")
    val major = c.downField("majorVersion")
    val minor = c.downField("minorVersion")
    val sender = c.downField("senderEmail")
    val mjApiKey = c.downField("mailjetApiKey")
    val mjSecretKey = c.downField("mailjetSecretKey")
    val id = c.key.get

    for {
      mep <- mobiEndPoint.as[String]
      ep <- endPoint.as[String]
      ak <- accesK.as[String]
      sk <- secretK.as[String]
      ma <- major.as[String]
      mi <- minor.as[String]
      s <- sender.as[String]
      mjak <- mjApiKey.as[String]
      mjsk <- mjSecretKey.as[String]
    } yield Config(id, mep, ep, ak, sk, ma, mi, s, mjak, mjsk)
  }


  def fromJson(configJson: String) = decode[Map[String, Config]](configJson) match {
    case Right(m: Map[String, Config])=> m
    case _=> Map()
  }
}
