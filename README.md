# Mobiliquest
A Mobiliscope service for building R queries on the fly

## Configuration file
Structure example:
```json
{
    "local": {
        "mobiliscopeEndPoint":"0.0.0.0:9005",
        "mobiliquestEndPoint": "0.0.0.0:9002",
        "minioEndPoint": "http://127.0.0.1:9000",
        "minioAccessKey": "minioadmin",
        "minioSecretKey": "miniopassword",
        "majorVersion": "vtest",
        "minorVersion": "20230706",
        "senderEmail": "mathieu.leclaire@iscpif.fr",
        "mailjetApiKey": "yourMailJetApiKey",
        "mailjetSecretKey":"yourMailJetApiSettings"
    },
    "test": {
    ...
    },
    "production": {
    ...
    }
}
```


## Run locally
### Start a local minio docker service
For instance, run the following docker-compose.yml:
```docker
version: '2'

services:
  minio:
    image: docker.io/bitnami/minio:2022
    ports:
      - '9000:9000'
      - '9001:9001'
    environment:
      - MINIO_ROOT_USER=mathieu
      - MINIO_ROOT_PASSWORD=mathieu-password
    volumes:
      - 'minio_data:/data'

volumes:
  minio_data:
    driver: local
```

### Start the Mobiliquest server
```console
sbt
sbt:mobiliquest> project server
sbt:mobiliquest> reStart /path/to/json/config/file
```

## Run with docker
### Mobiliquest docker image
A Mobiliquest docker images has been previously generated locally or on dockerhub


```shell
sbt
sbt:mobiliquest> project server
sbt:mobiliquest> docker:publish // or docker:publishLocal
```
### Docker-compose
```docker
services:
    mobiliquest:
      image: mobiliquest:0.1-SNAPSHOT
      container_name: mobiliquest
      ports:
        - 9002:9002
      privileged: true
      volumes:
       - '/data/minio:/data'
       - './config:/data/config'
```
```shell
> docker-compose up -d // docker-compose down
```
