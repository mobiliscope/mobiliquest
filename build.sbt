import com.typesafe.sbt.packager.docker._

val ScalaVersion = "3.3.1"
val Version = "1.0-SNAPSHOT"

val scalatagsVersion = "0.11.1"
val betterFilesVersion = "3.9.1"
val minioVersion = "8.5.4"
val json4sVersion = "4.0.6"
val http4sVersion = "0.23.25"
val circeVersion = "0.14.6"

val endpoints4SVersion = "1.8.0+n"
val endpointCirceVersion = "2.2.0+n"

lazy val shared = project.in(file("shared")) settings(
  scalaVersion := ScalaVersion,
  libraryDependencies ++= Seq(
    "org.openmole.endpoints4s" %%% "algebra" % endpoints4SVersion,
    "org.openmole.endpoints4s" %%% "json-schema-circe" % endpointCirceVersion,
    "io.circe" %% "circe-generic" % circeVersion
  )
) enablePlugins (ScalaJSPlugin)

lazy val server = project.in(file("server")) enablePlugins(DockerPlugin, JavaAppPackaging) settings(
  name := "Mobiliquest",
  version := Version,
  scalaVersion := ScalaVersion,
  dockerExposedPorts ++= Seq(8080),
  packageName := "mobiliquest",
  Compile / run / fork := true,
  dockerUsername := Some("mathieuleclaire"),
  dockerCommands ++= Seq(
    Cmd("FROM", "rocker/r2u:jammy"),
    Cmd("USER", "root"),
    Cmd("RUN",
      """apt-get update && \
           apt-get install -y curl \
           wget \
           openjdk-8-jdk"""),
    Cmd("ENV", "JAVA_HOME=/usr/lib/jvm/java-8-openjdk-amd64/jre/bin/java"),
    Cmd("COPY", "--from=mainstage /opt/docker/bin /opt/docker/bin"),
    Cmd("COPY", "--from=mainstage /opt/docker/lib /opt/docker/lib"),
    Cmd("RUN", """R --slave -e 'install.packages(c("stringr","dplyr","tidyr","purrr","sf","geojsonio","geojsonsf","jsonlite","OasisR","spdep","readxl"))'"""),
    Cmd("ENTRYPOINT", "/opt/docker/bin/mobiliquest")
  ),
  libraryDependencies ++= Seq(
    "org.ddahl" % "rscala_2.13" % "3.2.19",
    "com.lihaoyi" %% "scalatags" % scalatagsVersion,
    "org.openmole.endpoints4s" %% "http4s-server" % "10.0.0+n",
    "org.json4s" %% "json4s-jackson" % json4sVersion,
    "org.http4s" %% "http4s-circe" % http4sVersion,
    "io.circe" %% "circe-parser" % circeVersion,
    // Optional for auto-derivation of JSON codecs
    "io.circe" %% "circe-generic" % circeVersion,
    // Optional for string interpolation to JSON model
    "io.circe" %% "circe-literal" % circeVersion,
    "com.github.pathikrit" % "better-files-akka_2.13" % betterFilesVersion,
    "io.minio" % "minio" % minioVersion,
    "com.mailjet" % "mailjet-client" % "5.2.3",
    "org.http4s" %% "http4s-dsl" % http4sVersion,
    "org.http4s" %% "http4s-ember-server" % http4sVersion,
    "org.http4s" %% "http4s-ember-client" % http4sVersion,
    "org.typelevel" %% "log4cats-slf4j" % "2.1.1",
  )
) dependsOn (shared)